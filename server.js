//Proyecto techu4_practitioner - Trabajo Fina
// Participantes : Victoria Hernández - Jorge Sánchez
// 4ta Edición - Octubre 2018

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
//acceso seguro para MLAB
  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Credentials", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
    next();
  });

  var requestjson = require('request-json');


// Variables acceso Base de Datos
// Variables colecciones

var path = require('path');
var urlBaseMLab="https://api.mlab.com/api/1/databases/techu4_practitioner-project/collections/";
var apiKey="?apiKey=Y9VqSdfVAYzfc6vSGf9bdqGJcBotY0AT";
var prefijo = "/api/1";
var cUser = "Usuarios";
var cMov ="movimientos";
var cCtaMov ="clientes-productos";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// API's CRUD - usuarios

//Insertar cliente  --- Ok en Postman

app.post(prefijo+"/usuarios",function(req,res){
  //res.send('Hola Mundo Node');
  var data = req.body;
  console.log(data);
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.post(cUser+apiKey, data, function(errCliente,resCliente,bodyCliente){
    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

/*Consulta de idCliente-Password  -- Ok Postman */


app.get(prefijo+"/usuarios/:idCliente&:Password",function(req,res){

  var idCliente= req.params.idCliente;
  var pass= req.params.Password;

  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.get(cUser+apiKey+'&q={"$and": [{"idCliente": { "$eq": '+ idCliente +' } }, { "Password": { "$eq": "'+pass+'" }}]}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

//Consultar por idCliente -- Ok Postman

app.get(prefijo+"/usuarios/:idCliente",function(req,res){

  var idCliente= req.params.idCliente;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.get(cUser+apiKey+'&q={"idCliente":'+idCliente+'}', function(errCliente,resCliente,bodyCliente){
    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }
  });
});

/*
  Actualizar cliente -- Ok Postman
*/

app.put(prefijo+"/usuarios/:idCliente",function(req,res){
  var idCliente= req.params.idCliente;
  var data = req.body;
  var dataCompleto =  { "$set":{} };
  dataCompleto["$set"]=data;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.put(cUser+apiKey+'&q={"idCliente":'+idCliente+'}', dataCompleto, function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

/*
  Borrar cliente
  Datos de entrada idCliente  - Postman OK
*/

app.delete(prefijo+"/usuarios/:idCliente", function(req,res){

   var idCliente= req.params.idCliente;

   var clienteMLabP = requestjson.createClient(urlBaseMLab);
   clienteMLabP.get(cUser+apiKey+'&q={"idCliente":'+idCliente+'}'+'&fo=true', function(errClienteGet,resClienteGet,bodyClienteGet){

     if(errClienteGet){
        console.log(errClienteGet);
      } else {
        console.log(bodyClienteGet);
        var idMongo = bodyClienteGet._id['$oid'];

        clienteMLabP.del(cUser+"/"+idMongo+apiKey, function(errCliente,resCliente,bodyCliente){
          if(errCliente){
             console.log(errCliente);
           } else {
             res.send({"resultado":"Registro borrado"});
           }
        });

      }

   });
});



// API's CRUD - productos

// Agregar productos  -- ok Postman
app.post(prefijo+"/clientes-productos",function(req,res){

  var data = req.body;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.post(cCtaMov+apiKey, data, function(errCliente,resCliente,bodyCliente){
    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }
  });
});

// Consulta productos de un clientes

app.get(prefijo+"/clientes-productos/:idCliente",function(req,res){

  var idCliente= req.params.idCliente;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.get(cCtaMov+apiKey+'&q={"idCliente":'+idCliente+'}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }
});
});




// API's CRUD - movimientos

//Put - insertar movimientos -- Ok Postman

app.put(prefijo+"/movimientos/:Cliente&:cuenta",function(req,res){
  //res.send('Hola Mundo Node');
  var Cliente= req.params.Cliente;
  var cuenta=req.params.cuenta;

  var data = req.body;
  var dataCompleto =  { "$addToSet":{"movimientos":{}} };
  dataCompleto["$addToSet"]["movimientos"]=data;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.put(cMov+apiKey+'&q={"$and": [{"idCliente": { "$eq": '+ Cliente +' } }, { "idCuenta": { "$eq": '+cuenta+' }}]}', dataCompleto, function(errCliente,resCliente,bodyCliente){
    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

//Registra cabecera de cuenta en movimientos

app.post(prefijo+"/movimientos",function(req,res){
  //res.send('Hola Mundo Node');

  var data1 = req.body;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);

  console.log(req.body);
  clienteMLabP.post(cMov+apiKey, data1, function(errCliente,resCliente,bodyCliente){
    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});










//Consulta de movimientos de un producto
//Datos de entrada idCliente e idCuenta




app.get(prefijo+"/movimientos/:Cliente&:cuenta",function(req,res){

  var Cliente= req.params.Cliente;
  var cuenta= req.params.cuenta;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);

  clienteMLabP.get(cMov+apiKey+'&q={"$and": [{"idCliente": { "$eq": '+ Cliente +' } }, { "idCuenta": { "$eq": '+cuenta+' }}]}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }
  });
});

